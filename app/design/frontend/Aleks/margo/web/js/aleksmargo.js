define([
    'jquery',
    'jquery/ui',
    "matchMedia",
], function($) {
    "use strict";

    $.widget('mage.aleksmargo', {

        options: {
            toggleClass: 'active',
            parent: '.footer-links-container li.title'
        },

        _create: function() {
            mediaCheck({
                media: '(max-width: 640px)',
                entry: $.proxy(function () {
                    this._addEvent();
                }, this),
                exit: $.proxy(function () {
                    this._removeToggleClass();
                }, this)
            });
        },

        _addEvent: function() {
            this.element.on('click', $.proxy(function (e) {
                this._onClick();
                e.preventDefault();
            }, this));
        },

        _onClick: function() {
            this.element.toggleClass(this.options.toggleClass);
            $(this.element).siblings('ul.footer').toggle('300');
        },

        _removeToggleClass: function() {
            let toggleClass = this.options.toggleClass;
            $(this.options.parent).filter('.' + toggleClass).each(function () {
                $(this).removeClass(toggleClass);
            });
        },

    });

    return $.mage.aleksmargo;
});