<?php

namespace Magecloud\Testimonials\Setup;

use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        if (version_compare($context->getVersion(), '1.1.0') < 0) {

            $connection = $setup->getConnection();
            $connection->addIndex(
                $setup->getTable('magecloud_testimonials'),
                $setup->getIdxName('magecloud_testimonials', ['id']),
                ['id']
            );

        }

        if (version_compare($context->getVersion(), '1.2.0') < 0) {

            $connection = $setup->getConnection();

            $connection->changeColumn(
                'magecloud_testimonials', 'customer_id', 'customer_id', ['type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, 'unsigned' => true, 'nullable' => false, 'primary' => true,]
            );

            $connection->addForeignKey(
                $setup->getFkName('magecloud_testimonials', 'customer_id', 'customer_entity', 'entity_id'),
                $setup->getTable('magecloud_testimonials'),
                    'customer_id',
                $setup->getTable('customer_entity'),
                    'entity_id',
                    \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
                );
        }

        $setup->endSetup();
    }
}
