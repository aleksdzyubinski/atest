<?php

namespace Magecloud\Testimonials\Block;

class Button extends \Magento\Framework\View\Element\Template
{

    protected $helper;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magecloud\Testimonials\Helper\Data $helper)

    {
        $this->helper = $helper;
        parent::__construct($context);
    }

    public function isLoggedInCheck()
    {
        if($this->helper->getGeneralConfig('loggedin_only')) {
            if($this->helper->isLoggedIn()) {
                return true;
            } else {
                return false;
            }
        }
        return true;
    }
}