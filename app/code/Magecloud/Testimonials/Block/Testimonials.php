<?php
namespace Magecloud\Testimonials\Block;

class Testimonials extends \Magento\Framework\View\Element\Template
{

    protected $_testimonialFactory;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magecloud\Testimonials\Model\TestimonialFactory $testimonialFactory
    ){
        parent::__construct($context);
        $this->_testimonialFactory = $testimonialFactory;
    }

    public function getTestimonials()
    {
        $page = ($this->getRequest()->getParam('p'))? $this->getRequest()->getParam('p') : 1;
        $limit = ($this->getRequest()->getParam('limit'))? $this->getRequest()->getParam('limit') : 5;

        $collection = $this->_testimonialFactory->create()->getCollection();


        $collection->join(
                ['ct' =>  'customer_entity'],
                "main_table.customer_id = ct.entity_id",
                ['firstname', 'lastname']
            );

        $collection->setPageSize($limit);
        $collection->setCurPage($page);

        return $collection;
    }

    protected function _prepareLayout()
    {

        parent::_prepareLayout();
        $this->pageConfig->getTitle()->set(__('Magecloud Testimonials'));


        if ($this->getTestimonials()){
            $pager = $this->getLayout()->createBlock('Magento\Theme\Block\Html\Pager', 'magecloud.testimonials.pager')
                ->setAvailableLimit(array(5=>5,10=>10,20=>20))
                ->setShowPerPage(true)
                ->setCollection($this->getTestimonials());

            $this->setChild('pager', $pager);

            $this->getTestimonials()->load();
        }
        return $this;
    }

    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

}