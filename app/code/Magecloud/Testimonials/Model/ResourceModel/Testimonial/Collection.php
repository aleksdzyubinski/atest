<?php
namespace Magecloud\Testimonials\Model\ResourceModel\Testimonial;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'id';
    protected $_eventPrefix = 'magecloud_testimonials_collection';
    protected $_eventObject = 'testimonials_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Magecloud\Testimonials\Model\Testimonial', 'Magecloud\Testimonials\Model\ResourceModel\Testimonial');
    }

}
