<?php

namespace Magecloud\Testimonials\Model;

class Testimonial extends \Magento\Framework\Model\AbstractModel
{

    protected function _construct()
    {
        $this->_init('Magecloud\Testimonials\Model\ResourceModel\Testimonial');
    }

}