<?php
namespace Magecloud\Testimonials\Controller\Index;

class Submit extends \Magento\Framework\App\Action\Action
{
    protected $customerSession;

    protected $testimonialFactory;

    protected $resultJsonFactory;

    protected $testimonialResourceModel;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magecloud\Testimonials\Model\TestimonialFactory $testimonialFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magecloud\Testimonials\Model\ResourceModel\Testimonial $testimonialResourceModel
    ) {
        parent::__construct($context);
        $this->customerSession = $customerSession;
        $this->testimonialFactory = $testimonialFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->testimonialResourceModel = $testimonialResourceModel;
    }

    public function execute()
    {
        $resultJson = $this->resultJsonFactory->create();
        $data = $this->getRequest()->getPostValue();

        if ((!$data) || (!\Zend_Validate::is(trim($data['testimonial_text']), 'NotEmpty'))) {

            $response = ['error' => 1, 'message' => 'Data is missing'];

        } else {

            try {
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['customer_id'] = $this->customerSession->getCustomer()->getId();
                $testimonial = $this->testimonialFactory->create()->setData($data);
                $this->testimonialResourceModel->save($testimonial);

                $response = ['error' => 0, 'message' => 'Thank you for testimonial!'];

            } catch(\Exception $e) {
                $response = ['error' => 1, 'message' => $e->getMessage()];
            }
        }

        return $resultJson->setData($response);
    }
}