<?php

namespace Magecloud\Testimonials\Controller\Adminhtml\Grid;

class Index extends \Magento\Backend\App\Action
{
    const ACL_RESOURCE = 'Magecloud_Testimonials::testimonials_grid';
    const MENU_ITEM = 'Magecloud_Testimonials::testimonials_grid';
    const TITLE = 'Magecloud Testimonials Grid';

    protected $resultPageFactory = false;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    )
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->prepend((__('Magecloud Testimonials')));

        return $resultPage;
    }
}

