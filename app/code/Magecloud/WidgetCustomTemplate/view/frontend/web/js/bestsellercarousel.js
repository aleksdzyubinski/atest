define([
    'jquery',
    'Magecloud_WidgetCustomTemplate/js/owlcarousel/owl.carousel.min'
], function ($) {
    'use strict';

    $('.bestsellers-owlcarousel').owlCarousel({
        loop: true,
        items: 4,
        nav: true,
        dots: false,
        navText: ["<i class='fas fa-chevron-left'></i>", "<i class='fas fa-chevron-right'></i>"],
        responsive:{
            0:{
                items:1,
            },
            480:{
                items:2,
            },
            767:{
                items:3,
            },
            1000:{
                items:4,
            }
        }
    });
});